﻿using System;
using System.Reflection;

namespace _2._13_HomeWork_Reflection_Zverev
{
    public class MyHandlerWithChild
    {
            public static string Serialize(object obj)
            {
                string result = String.Empty;
                Type objType = obj.GetType();
                foreach (FieldInfo field in objType.GetFields())
                {
                    var value = field?.GetValue(obj);
                    //if (!field.FieldType.IsPrimitive)
                    //{
                    //    result += $"[{field.Name}]({MyHandler.ChildFieldSerialize(value)})";
                    //    continue;
                    //}
                    result += $"[{field.Name}]({value})";
                }
                return result + ";";
            }

            //private static string ChildFieldSerialize(object obj)
            //{
            //    string result = String.Empty;
            //    Type objType = obj.GetType();
            //    foreach (FieldInfo field in objType.GetFields())
            //    {
            //        var value = field?.GetValue(obj);
            //        if (field.FieldType.IsPrimitive == false)
            //        {
            //            result += $"[{MyHandler.ChildFieldSerialize(value)}]";
            //        }
            //        result += $"[{field.Name}]({value})";
            //    }
            //    return result;
            //}



            public static List<T> Deserialize<T>(string inputString)
            {
                List<T> listObj = new List<T>();
                var lstInputClass = new List<string>(inputString.Split(";"));
                int classCount = 0;
                foreach (string inputClass in lstInputClass)
                {
                    classCount++;
                    if (classCount == lstInputClass.Count) { break; }
                    Type type = typeof(T);
                    object obj = Activator.CreateInstance(type);

                    bool fieldNameStarted = false;
                    bool fieldNameEnd = false;

                    bool fieldValueStarted = false;
                    bool fieldValueEnd = false;

                    //bool childStarted = true;
                    //bool childEnd = true;

                    string fieldName = String.Empty;
                    string fieldValue = String.Empty;
                    //string child = String.Empty;

                    int step = 0;
                    foreach (char ch in inputClass)
                    {
                        step++;

                        if (fieldNameStarted && fieldValueEnd &&
                            fieldValueStarted && fieldValueEnd)
                        {
                            FieldInfo field = type.GetField(fieldName);
                            int fieldValueInt;
                            if (!int.TryParse(fieldValue, out fieldValueInt)) throw new Exception("Это не число");
                            field.SetValue(obj, fieldValueInt);


                            fieldNameStarted = false;
                            fieldNameEnd = false;
                            fieldName = String.Empty;

                            fieldValueStarted = false;
                            fieldValueEnd = false;
                            fieldValue = String.Empty;
                        }


                        //Собираем имя
                        if (!fieldNameStarted && ch == '[')
                        {
                            fieldNameStarted = true;
                            continue;
                        }
                        if (!fieldNameEnd && ch == ']')
                        {
                            fieldNameEnd = true;
                            continue;
                        }
                        if (!fieldNameEnd)
                        {
                            fieldName += ch;
                            continue;
                        }

                        //Проверяем дочерние значения
                        //if (fieldValueStarted && ch == '[')
                        //{
                        //    childStarted = true;
                        //    child += ch;
                        //    continue
                        //}
                        //


                        //Собираем значение
                        if (!fieldValueStarted && ch == '(')
                        {
                            fieldValueStarted = true;
                            continue;
                        }
                        if (!fieldValueEnd && ch == ')')
                        {
                            fieldValueEnd = true;
                            if (step == inputClass.Length)
                            {
                                FieldInfo field = type.GetField(fieldName);
                                int fieldValueInt;
                                if (!int.TryParse(fieldValue, out fieldValueInt)) throw new Exception("Это не число");
                                field.SetValue(obj, fieldValueInt);
                            }
                            continue;
                        }
                        if (!fieldValueEnd)
                        {
                            fieldValue += ch;
                            continue;
                        }
                    }
                    listObj.Add((T)obj);

                }

                return listObj;
            }


            //public static string DeserializeChild<T>(char inputChar)
            //{
            //    bool fieldNameStarted = false;
            //    bool fieldNameEnd = false;

            //    bool fieldValueStarted = false;
            //    bool fieldValueEnd = false;

            //    string fieldName = String.Empty;
            //    string fieldValue = String.Empty;
            //    foreach (char ch in inputClass)
            //    {
            //        if (fieldNameStarted && fieldValueEnd &&
            //            fieldValueStarted && fieldValueEnd)
            //        {
            //            Console.WriteLine($"Имя свойства: {fieldName}");
            //            Console.WriteLine($"Значение: {fieldValue}");
            //            Console.WriteLine("-----------------------");

            //            fieldNameStarted = false;
            //            fieldNameEnd = false;
            //            fieldName = String.Empty;

            //            fieldValueStarted = false;
            //            fieldValueEnd = false;
            //            fieldValue = String.Empty;
            //        }


            //        //Собираем имя
            //        if (!fieldNameStarted && ch == '[')
            //        {
            //            fieldNameStarted = true;
            //            continue;
            //        }
            //        if (!fieldNameEnd && ch == ']')
            //        {
            //            fieldNameEnd = true;
            //            continue;
            //        }
            //        if (!fieldNameEnd)
            //        {
            //            fieldName += ch;
            //            continue;
            //        }

            //        //Проверяем дочерние значения
            //        if (fieldValueStarted && ch == '[')
            //        {

            //        }


            //        //Собираем значение
            //        if (!fieldValueStarted && ch == '(')
            //        {
            //            fieldValueStarted = true;
            //            continue;
            //        }
            //        if (!fieldValueEnd && ch == ')')
            //        {
            //            fieldValueEnd = true;
            //            continue;
            //        }
            //        if (!fieldValueEnd)
            //        {
            //            fieldValue += ch;
            //            continue;
            //        }
            //    }
            //    }



        
    }
}

